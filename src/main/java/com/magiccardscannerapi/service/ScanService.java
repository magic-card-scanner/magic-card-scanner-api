package com.magiccardscannerapi.service;

import com.magiccardscannerapi.magiccardscannerocr.MagicCardScannerOcr;
import com.magiccardscannerapi.magiccardscannerocr.view.CardScanResult;
import com.magiccardscannerapi.rest.exception.CardScanFailedException;
import com.magiccardscannerapi.rest.exception.NoCardFoundException;
import com.magiccardscannerapi.rest.view.CardView;
import com.magiccardscannerapi.scryfallapi.ScryfallApi;
import com.magiccardscannerapi.scryfallapi.view.Card;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
public class ScanService {

    private final MagicCardScannerOcr magicCardScannerOcr;
    private final ScryfallApi scryfallApi;

    public ScanService(MagicCardScannerOcr magicCardScannerOcr, ScryfallApi scryfallApi) {
        this.magicCardScannerOcr = magicCardScannerOcr;
        this.scryfallApi = scryfallApi;
    }

    public CardView scanCard(MultipartFile file) {
        final CardScanResult scan;
        try {
            scan = magicCardScannerOcr.scanCard(file);
        } catch (final FeignException e) {
            log.error("Unable to recognize card with filename : {}", file.getOriginalFilename());
            throw new CardScanFailedException("Cannot recognize card");
        }

        final String set = scan.getSet();
        final String collectionNumber = scan.getCollectionNumber();
        log.info("Get card with set : {} and collectionNumber : {}", set, collectionNumber);
        final Card card;
        try {
            card = scryfallApi.getCardSBySetAndCollectionNumber(set, collectionNumber);
        } catch (final FeignException e) {
            log.error(
                    "Unable to find card in scryfall for the requested card set : {} and collectionNumber : {}",
                    set,
                    collectionNumber);
            throw new NoCardFoundException(
                    "Unable to find card with set : " + set + " and collectionNumber : " + collectionNumber);
        }
        return new CardView(card);
    }
}
