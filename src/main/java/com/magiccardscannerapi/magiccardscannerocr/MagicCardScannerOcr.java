package com.magiccardscannerapi.magiccardscannerocr;

import com.magiccardscannerapi.magiccardscannerocr.view.CardScanResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(value = "magicCardScannerOcr", url = "${application.magic-card-scanner-ocr.url}")
public interface MagicCardScannerOcr {

    @PostMapping(value = "/scans", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    CardScanResult scanCard(@RequestPart("file") MultipartFile file);
}
