package com.magiccardscannerapi.magiccardscannerocr.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CardScanResult {

    @JsonProperty("set")
    String set;

    @JsonProperty("collection_number")
    String collectionNumber;
}
