package com.magiccardscannerapi.rest.view;

import com.magiccardscannerapi.scryfallapi.view.Purchase;
import lombok.Value;

@Value
public class PurchaseView {

    String tcgplayer;
    String cardmarket;
    String cardhoarder;

    public PurchaseView(Purchase purchase) {
        this.tcgplayer = purchase.getTcgplayer();
        this.cardmarket = purchase.getCardmarket();
        this.cardhoarder = purchase.getCardhoarder();
    }
}
