package com.magiccardscannerapi.rest.view;

import com.magiccardscannerapi.scryfallapi.view.Image;
import lombok.Value;

@Value
public class ImageView {

    String small;
    String normal;
    String large;

    public ImageView(Image image) {
        this.small = image.getSmall();
        this.normal = image.getNormal();
        this.large = image.getLarge();
    }
}
