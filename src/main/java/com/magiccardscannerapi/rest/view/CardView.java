package com.magiccardscannerapi.rest.view;

import com.magiccardscannerapi.scryfallapi.view.Card;
import lombok.Value;

@Value
public class CardView {

    String id;
    String cardName;
    String printName;
    String releaseDate;
    String lang;
    ImageView images;
    String set;
    String setName;
    String collectionNumber;
    PriceView prices;
    PurchaseView purchases;

    public CardView(Card card) {
        this.id = card.getId();
        this.cardName = card.getCardName();
        this.printName = card.getPrintName();
        this.releaseDate = card.getReleaseDate();
        this.lang = card.getLang();
        this.images = new ImageView(card.getImage());
        this.set = card.getSet();
        this.setName = card.getSetName();
        this.collectionNumber = card.getCollectionNumber();
        this.prices = new PriceView(card.getPrice());
        this.purchases = new PurchaseView(card.getPurchase());
    }
}
