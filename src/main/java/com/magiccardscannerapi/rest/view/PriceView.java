package com.magiccardscannerapi.rest.view;

import com.magiccardscannerapi.scryfallapi.view.Price;
import lombok.Value;

@Value
public class PriceView {

    String usd;
    String usdFoil;
    String eur;
    String eurFoil;
    String tix;

    public PriceView(Price price) {
        this.usd = price.getUsd();
        this.usdFoil = price.getUsdFoil();
        this.eur = price.getEur();
        this.eurFoil = price.getEurFoil();
        this.tix = price.getTix();
    }
}
