package com.magiccardscannerapi.rest.resource;

import com.magiccardscannerapi.rest.view.CardView;
import com.magiccardscannerapi.service.ScanService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
@Slf4j
@Tag(name = "Scans", description = "Actions about magic card scan")
public class ScanResource {

    private final ScanService scanService;

    public ScanResource(ScanService scanService) {
        this.scanService = scanService;
    }

    @PostMapping(value = "/scans", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Scan a magic the gathering card")
    public CardView scanCard(@RequestPart("file") MultipartFile file) {
        return scanService.scanCard(file);
    }
}
