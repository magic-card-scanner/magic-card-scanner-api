package com.magiccardscannerapi.rest.exception;

import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoCardFoundException extends NoSuchElementException {
    public NoCardFoundException(String message) {
        super(message);
    }
}
