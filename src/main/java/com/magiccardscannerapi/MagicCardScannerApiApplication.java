package com.magiccardscannerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MagicCardScannerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicCardScannerApiApplication.class, args);
    }
}
