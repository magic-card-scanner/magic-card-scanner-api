package com.magiccardscannerapi.scryfallapi;

import com.magiccardscannerapi.scryfallapi.view.Card;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "scryfall", url = "${application.scryfall-api.url}")
public interface ScryfallApi {

    @GetMapping("/cards/{set}/{collectionNumber}")
    Card getCardSBySetAndCollectionNumber(
            @PathVariable("set") final String set,
            @PathVariable("collectionNumber") final String collectionNumber);
}
