package com.magiccardscannerapi.scryfallapi.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class Card {

    String id;
    String cardName;
    String printName;
    String releaseDate;
    String lang;
    Image image;
    String set;
    String setName;
    String collectionNumber;
    Price price;
    Purchase purchase;

    @JsonCreator
    public Card(
            @JsonProperty("id") String id,
            @JsonProperty("name") String cardName,
            @JsonProperty("printed_name") String printName,
            @JsonProperty("released_at") String releaseDate,
            @JsonProperty("lang") String lang,
            @JsonProperty("image_uris") Image image,
            @JsonProperty("set") String set,
            @JsonProperty("set_name") String setName,
            @JsonProperty("collector_number") String collectionNumber,
            @JsonProperty("prices") Price price,
            @JsonProperty("purchase_uris") Purchase purchase) {
        this.id = id;
        this.cardName = cardName;
        this.printName = printName;
        this.releaseDate = releaseDate;
        this.lang = lang;
        this.image = image;
        this.set = set;
        this.setName = setName;
        this.collectionNumber = collectionNumber;
        this.price = price;
        this.purchase = purchase;
    }
}
