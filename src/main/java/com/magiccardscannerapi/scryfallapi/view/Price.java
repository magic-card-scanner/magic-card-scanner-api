package com.magiccardscannerapi.scryfallapi.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class Price {

    @JsonProperty("usd")
    String usd;

    @JsonProperty("usd_foil")
    String usdFoil;

    @JsonProperty("eur")
    String eur;

    @JsonProperty("eur_foil")
    String eurFoil;

    @JsonProperty("tix")
    String tix;
}
