package com.magiccardscannerapi.scryfallapi.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class Purchase {

    @JsonProperty("tcgplayer")
    String tcgplayer;

    @JsonProperty("cardmarket")
    String cardmarket;

    @JsonProperty("cardhoarder")
    String cardhoarder;
}
