package com.magiccardscannerapi.scryfallapi.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class Image {

    @JsonProperty("small")
    String small;

    @JsonProperty("normal")
    String normal;

    @JsonProperty("large")
    String large;
}
