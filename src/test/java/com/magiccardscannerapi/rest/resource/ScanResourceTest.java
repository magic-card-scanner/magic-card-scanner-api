package com.magiccardscannerapi.rest.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.magiccardscannerapi.rest.view.CardView;
import com.magiccardscannerapi.scryfallapi.view.Card;
import com.magiccardscannerapi.scryfallapi.view.Image;
import com.magiccardscannerapi.scryfallapi.view.Price;
import com.magiccardscannerapi.scryfallapi.view.Purchase;
import com.magiccardscannerapi.service.ScanService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@AutoConfigureMockMvc
@SpringBootTest
class ScanResourceTest {

    @Autowired private MockMvc mockMvc;

    @MockBean ScanService scanService;

    @Test
    void whenScanCard_givenCorrectCardInformation() throws Exception {
        // GIVEN
        final MockMultipartFile file =
                new MockMultipartFile("file", "file.png", "text/plain", "some xml".getBytes());
        final Image image = new Image("small", "normal", "large");
        final Price price = new Price("10", "20", "30", "40", "0");
        final Purchase purchase = new Purchase("tcgplayer", "cardmarket", "cardhoarder");
        final Card card =
                new Card(
                        "00cbe506-7332-4d29-9404-b7c6e1e791d8",
                        "LilianaoftheDarkRealms",
                        null,
                        "2013-07-19",
                        "en",
                        image,
                        "m14",
                        "Magic2014",
                        "102",
                        price,
                        purchase);

        when(scanService.scanCard(file)).thenReturn(new CardView(card));

        // WHEN
        final MvcResult mvcResult =
                mockMvc
                        .perform(MockMvcRequestBuilders.multipart("/scans").file(file))
                        .andExpect(status().isOk())
                        .andReturn();

        // THEN
        final String content = mvcResult.getResponse().getContentAsString();
        assertEquals(
                "{"
                        + "\"id\":\"00cbe506-7332-4d29-9404-b7c6e1e791d8\","
                        + "\"cardName\":\"LilianaoftheDarkRealms\","
                        + "\"printName\":null,"
                        + "\"releaseDate\":\"2013-07-19\","
                        + "\"lang\":\"en\","
                        + "\"images\":{"
                        + "\"small\":\"small\","
                        + "\"normal\":\"normal\","
                        + "\"large\":\"large\"},"
                        + "\"set\":\"m14\","
                        + "\"setName\":\"Magic2014\","
                        + "\"collectionNumber\":\"102\","
                        + "\"prices\":{"
                        + "\"usd\":\"10\","
                        + "\"usdFoil\":\"20\","
                        + "\"eur\":\"30\","
                        + "\"eurFoil\":\"40\","
                        + "\"tix\":\"0\"},"
                        + "\"purchases\":{"
                        + "\"tcgplayer\":\"tcgplayer\","
                        + "\"cardmarket\":\"cardmarket\","
                        + "\"cardhoarder\":\"cardhoarder\"}}",
                content);
    }
}
