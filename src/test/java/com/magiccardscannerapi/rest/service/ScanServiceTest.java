package com.magiccardscannerapi.rest.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.magiccardscannerapi.magiccardscannerocr.MagicCardScannerOcr;
import com.magiccardscannerapi.magiccardscannerocr.view.CardScanResult;
import com.magiccardscannerapi.rest.exception.CardScanFailedException;
import com.magiccardscannerapi.rest.exception.NoCardFoundException;
import com.magiccardscannerapi.rest.view.CardView;
import com.magiccardscannerapi.scryfallapi.ScryfallApi;
import com.magiccardscannerapi.scryfallapi.view.Card;
import com.magiccardscannerapi.scryfallapi.view.Image;
import com.magiccardscannerapi.scryfallapi.view.Price;
import com.magiccardscannerapi.scryfallapi.view.Purchase;
import com.magiccardscannerapi.service.ScanService;
import feign.FeignException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

@AutoConfigureMockMvc
@SpringBootTest
class ScanServiceTest {

    @Autowired ScanService scanService;

    @MockBean MagicCardScannerOcr magicCardScannerOcr;

    @MockBean ScryfallApi scryfallApi;

    @Test
    void whenScanCard_givenCorrectCardInformation() {
        // GIVEN
        final MockMultipartFile file =
                new MockMultipartFile("file", "file.png", "text/plain", "some xml".getBytes());

        final CardScanResult cardScanResult = new CardScanResult("m14", "102");
        final String set = cardScanResult.getSet();
        final String collectionNumber = cardScanResult.getCollectionNumber();

        final Image image = new Image("small", "normal", "large");
        final Price price = new Price("10", "20", "30", "40", "0");
        final Purchase purchase = new Purchase("tcgplayer", "cardmarket", "cardhoarder");
        final Card card =
                new Card(
                        "00cbe506-7332-4d29-9404-b7c6e1e791d8",
                        "LilianaoftheDarkRealms",
                        null,
                        "2013-07-19",
                        "en",
                        image,
                        "m14",
                        "Magic2014",
                        "102",
                        price,
                        purchase);

        when(magicCardScannerOcr.scanCard(file)).thenReturn(cardScanResult);
        when(scryfallApi.getCardSBySetAndCollectionNumber(set, collectionNumber)).thenReturn(card);

        // WHEN
        final CardView result = scanService.scanCard(file);

        // THEN
        assertEquals(result.getCardName(), "LilianaoftheDarkRealms");
    }

    @Test
    void whenScanCard_givenWrongCardInformationThrowCardScanFailedException() {
        // GIVEN
        final MockMultipartFile wrongFile =
                new MockMultipartFile("file", "wrongFile.png", "text/plain", "some xml".getBytes());

        when(magicCardScannerOcr.scanCard(wrongFile)).thenThrow(FeignException.class);

        // THEN
        assertThrows(CardScanFailedException.class, () -> scanService.scanCard(wrongFile));
    }

    @Test
    void whenScanCard_givenWrongCardInformationThrowNoCardFoundException() {
        // GIVEN
        final MockMultipartFile wrongFile =
                new MockMultipartFile("file", "wrongFile.png", "text/plain", "some xml".getBytes());

        final CardScanResult cardScanResult = new CardScanResult("m14", "102");

        when(magicCardScannerOcr.scanCard(wrongFile)).thenReturn(cardScanResult);
        when(scryfallApi.getCardSBySetAndCollectionNumber(anyString(), anyString()))
                .thenThrow(FeignException.class);

        // THEN
        assertThrows(NoCardFoundException.class, () -> scanService.scanCard(wrongFile));
    }
}
